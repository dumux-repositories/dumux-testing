dune_add_test(NAME test_coverage_print
              SOURCES main.cc)

dune_add_test(NAME test_coverage_print_runtime
              TARGET test_coverage_print
              COMMAND ./test_coverage_print
              CMD_ARGS -PrintGerman true)

dune_add_test(NAME test_coverage_print_compiletime
              SOURCES main.cc
              COMPILE_DEFINITIONS USEGERMAN=1)
