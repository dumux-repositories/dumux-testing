#include <config.h>

#include <dumux/common/parameters.hh>

#include "example.hh"

#ifndef USEGERMAN
#define USEGERMAN 0
#endif

int main(int argc, char** argv)
{
    using namespace Dumux;

    Parameters::init(argc, argv);

    Printer<USEGERMAN> printer;
    printer.print();

    if (getParam<bool>("PrintGerman", false))
    {
        std::cout << "Dumux was tested successfully." << std::endl;
    }
    else
    {
        std::cout << "Dumux wurde erfolreich getested." << std::endl;
    }

    return 0;
}
