#ifndef DUMUX_COVERAGE_TEST_EXAMPLE_HH
#define DUMUX_COVERAGE_TEST_EXAMPLE_HH

#include <iostream>

namespace Dumux {

template<bool useGerman = false>
struct Printer;

template<>
struct Printer<false>
{
    void print() const
    {
        std::cout << "Hello World!" << std::endl;
    }
};

template<>
struct Printer<true>
{
    void print() const
    {
        std::cout << "Hallo Welt!" << std::endl;
    }
};

} // end namespace Dumux

#endif
