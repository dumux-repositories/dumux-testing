#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone -b releases/2.8 https://gitlab.dune-project.org/staging/dune-uggrid
git clone -b releases/2.8 https://gitlab.dune-project.org/extensions/dune-alugrid

git clone -b releases/2.8 https://gitlab.dune-project.org/quality/dune-testtools
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-testing.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git

### Out of source building and running all tests in dumux-testing
mkdir buildtests
./dune-common/bin/dunecontrol --builddir=$PWD/buildtests --opts=dumux-testing/debug.opts all
cd buildtests/dumux-testing
ninja build_tests
ctest
