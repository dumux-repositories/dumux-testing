Description
=============

This module includes a so-called systemtest which uses a special [ini-file](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-testing/-/blob/master/systemtest/porousmediumflow/1p/test_1pfv.ini) where various compile-time properties with multiple values can be set. Based on these properties and their values, a test for each possible combination of their values is built. By doing so, a wide range of functionality can be tested using only one ini-file.

Installation
==============

Instructions for running the systemtest using out-of-source building (for local testing):
1) Create a folder where all modules should be downloaded to, e.g. the folder "test_DumuxTesting".
2) Enter the created folder. You can download the [installscript](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-testing/-/blob/master/installdumuxtesting.sh) via
```
wget https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-testing/-/raw/master/installdumuxtesting.sh
```  

3) Running the installscript via
```
./installdumuxtesting
```

will download all necessary dune and dumux modules, create an out-of-source build directory called `buildtests`, enter the out-of-source build folder of dumux-testing `buildtests/dumux-testing`, build the tests via `ninja build_tests` and execute them via `ctest`. The necessary coverage-flags are set in [debug.opts](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-testing/-/blob/master/debug.opts).
